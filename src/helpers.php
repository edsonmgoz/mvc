<?php

/**
 * Helpers...
 */

/**
 * Call static files from assets folder
 * @param  string $resource string to resource
 * @return string full URL to asset
 */
function assets($resource)
{
	return APP_DOMAIN . '/assets/' . $resource;
}

/**
 * Debug method
 * @param  array $data
 * @return void
 */
function p($data)
{
	die('<pre>'. print_r($data, 1));
}

/**
 * redirect to a view
 * @param string  $url
 */
function redirect($url)
{
    header('Location: ' . $url);
    exit();
}

/**
 * Convert Array to <ul> list
 * @param  $data data to generate list
 */
function toList($data = false)
{
	$response = '<ul>';
	if(false !== $data)
	{
		foreach($data as $key=>$val)
		{
			$response.= '<li>';
			if(!is_array($val))
			{
				$response.= $val;
			}
			$response.= '</li>';
		}
	}
	$response.= '</ul>';
	return $response;
}