<?php
namespace App\Core;

use Exception;

class Template
{
    private $viewPath = '%s/src/View';
    private $layout = 'layout.phtml';
    private $reservedVariables = ['application_name', 'body'];

    public function __construct()
    {
        $this->viewPath = sprintf($this->viewPath, APP_ROOT);
    }

    public function getView($controller, array $variables = [])
    {
        $variables = $this->validateVariables($variables);

        $parts = explode('::', $controller);
        $directory = $this->getDirectory($parts[0]);
        $file = $this->getFile($parts[1]);

        $viewPath = $this->viewPath . '/' . $directory . '/' . $file . '.phtml';
        if (file_exists($viewPath)) {
            $layoutPath = $this->viewPath . '/' . $this->layout;

            ob_start();
            $body = $viewPath;
            array_push($variables, $body);
            extract($variables);
            include $layoutPath;

            return ob_get_clean();

        }

        http_response_code(404);
        throw new Exception(sprintf('View cannot be found: [%s]', $viewPath), 404);
    }

    private function validateVariables(array $variables = [])
    {
        foreach ($variables as $name => $value) {
            if (in_array($name, $this->reservedVariables)) {
                http_response_code(404);
                throw new Exception('Unacceptable view variable given: [body]', 409);
            }
        }

        $variables['application_name'] = APP_NAME;

        return $variables;
    }

    private function getDirectory($controller)
    {
        $parts = explode('\\', $controller);
        $cleanDirectory = str_replace('Controller', '', end($parts));

        return $cleanDirectory;
    }

    private function getFile($controller)
    {
        return str_replace(APP_CONTROLLER_METHOD_SUFFIX, null, $controller);
    }
}