<?php
namespace App\Controller;

use App\Core\Template;
use Plasticbrain\FlashMessages\FlashMessages;

/**
* Main Controller Class
*/
class Controller
{
    private $template;
	public $msg;

    public function __construct()
    {
        $this->template = new Template();
        if (!session_id()) @session_start();
        $this->msg = new FlashMessages();
    }

    protected function getView($controller, array $variables = [])
    {
        return $this->template->getView($controller, $variables);
    }
}