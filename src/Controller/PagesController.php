<?php
namespace App\Controller;

use App\Core\Template;

class PagesController extends Controller
{
    public function __construct()
    {
        parent::__construct(new Template());
    }

    public function indexAction()
    {
        return parent::getView(
            __METHOD__,
            [
                'title'  => APP_NAME . ' - Pages'
            ]
        );

    }
}