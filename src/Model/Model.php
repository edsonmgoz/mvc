<?php
namespace App\Model;

use PDO;

/**
* Main Model Class
*/
class Model {

	public $dbh;

	public function __construct()
	{
		$this->dbh = new PDO('pgsql:host=' . DB_HOST . ';port=5432;dbname=' . DB_NAME . ';user=' . DB_USER . ';password='. DB_PASS . '');
	}
}