<?php
define('APP_NAME', 'My App');
define('APP_DOMAIN', 'http://mvc.app');
define('APP_INNER_DIRECTORY', '/mix/mvc');
define('APP_ROOT', __DIR__ . '/..');

define('SECRET', 'your_secret_key_here');

define('APP_CONTROLLER_NAMESPACE', 'App\\Controller\\');
define('APP_DEFAULT_CONTROLLER', 'Pages');
define('APP_DEFAULT_CONTROLLER_METHOD', 'index');
define('APP_CONTROLLER_METHOD_SUFFIX', 'Action');

define('DB_HOST', 'localhost');
define('DB_NAME', 'mvc');
define('DB_USER', 'root');
define('DB_PASS', 'password');