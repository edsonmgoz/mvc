# MVC Wrapper

## Installation

1. git clone https://gitlab.com/edsonmgoz/mvc.git
2. Run `composer install` into the project folder.

## Configuration

Read and edit `app/config.php` and setup the configuration relevant for your application.
Important execute the project from a virtual host into de `public` folder. Example for a virtual host: `http://mvc.app`

## Packages included

- Flash Messages: https://github.com/plasticbrain/PhpFlashMessages
- Validation: https://github.com/Respect/Validation

## TODO:
1. Improve validation for a specific Model.
2. Recover field data after validation.